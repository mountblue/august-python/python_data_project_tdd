import csv
import matplotlib.pyplot as plt


def get_matches_per_season(matches_csv_path):
    matches_per_season = {}
    with open(matches_csv_path, 'r') as matches_csv_file:
        matches_reader = csv.DictReader(matches_csv_file)
        for match in matches_reader:
            if int(match["season"]) not in matches_per_season.keys():
                matches_per_season[int(match["season"])] = 1
            else:
                matches_per_season[int(match["season"])] += 1
    return matches_per_season


def plot_on_graph():
    matches_per_season = get_matches_per_season("matches.csv")
    plt.bar(range(len(matches_per_season.keys())), [matches_per_season[season] for season in sorted(matches_per_season.keys())])
    plt.xticks(range(len(matches_per_season.keys())), sorted(matches_per_season.keys()))
    plt.title("Matches Per Year")
    plt.xlabel("Season")
    plt.ylabel("Number of Matches")
    plt.show()

if __name__ == '__main__':
    plot_on_graph()