import csv
import matplotlib.pyplot as plt

# returns a nested dictionary with season as outer key and team_name as inner key
def get_matches_won_by_all_teams_over_all_seasons(matches_csv_path):
    matches_won_by_all_teams = {}

    with open(matches_csv_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for match in matches_reader:
            if match["winner"] not in matches_won_by_all_teams.keys() and match["winner"] is not '':
                matches_won_by_all_teams[match["winner"]] = {2008: 0, 2009: 0, 2010: 0, 2011: 0,
                                                             2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 0, 2017: 0}
                matches_won_by_all_teams[match["winner"]
                                         ][int(match["season"])] += 1
            elif match["winner"] is not '':
                matches_won_by_all_teams[match["winner"]
                                         ][int(match["season"])] += 1
    return matches_won_by_all_teams


def plot_stacked_bar_chart():
    matches_won_by_all_teams = get_matches_won_by_all_teams_over_all_seasons(
        "matches.csv")
    
    team_total = None

    for team_name in matches_won_by_all_teams.keys():
        if team_total is None:
            team_total = matches_won_by_all_teams[team_name].values()
        else:
            team_total = [int(x) + int(y)
                          for x, y in zip(matches_won_by_all_teams[team_name].values(), team_total)]

    for team_name in matches_won_by_all_teams.keys():
        plt.bar(range(len(matches_won_by_all_teams[team_name].keys())), team_total, label=team_name)
        team_total = [int(y) - int(x)
                      for x, y in zip(matches_won_by_all_teams[team_name].values(), team_total)]

    plt.xticks(range(len(matches_won_by_all_teams[team_name].keys())), matches_won_by_all_teams[team_name].keys())
    plt.legend(loc="upper right", fontsize="x-small", bbox_to_anchor = (1.12, 1))
    plt.title("Matches won by each team over years")
    plt.xlabel("Season")
    plt.ylabel("Number of Matches")
    plt.show()


if __name__ == '__main__':
    plot_stacked_bar_chart()