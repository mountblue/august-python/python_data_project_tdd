import csv
import matplotlib.pyplot as plt


def get_match_ids_of_2016(matches_csv_path):
    season = 2016
    ids_of_2016 = set()
    with open(matches_csv_path, 'r') as matches_csv_file:
        matches_reader = csv.DictReader(matches_csv_file)
        for match in matches_reader:
            if int(match["season"]) == season:
                ids_of_2016.add(int(match["id"]))
    return ids_of_2016


def get_extras_by_team(deliveries_csv_path, season_ids):
    extras_per_team = {}
    with open(deliveries_csv_path) as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        for delivery in deliveries_reader:
            if delivery["bowling_team"] not in extras_per_team.keys() and int(delivery['match_id']) in season_ids:
                extras_per_team[delivery["bowling_team"]] = int(
                    delivery["extra_runs"])
            elif int(delivery['match_id']) in season_ids:
                extras_per_team[delivery["bowling_team"]
                                ] += int(delivery["extra_runs"])
    return extras_per_team


def plot_bar_chart():
    ids_of_2016 = get_match_ids_of_2016("matches.csv")
    extras_per_team = get_extras_by_team("deliveries.csv", ids_of_2016)
    plt.bar(extras_per_team.keys(), extras_per_team.values())
    plt.xticks(rotation=15)
    plt.title("Extra runs per team")
    plt.xlabel("Season")
    plt.ylabel("Extras")
    plt.show()


if __name__ == '__main__':
    plot_bar_chart()