import csv
import matplotlib.pyplot as plt


def get_match_ids_of_2015(matches_csv_path):
    match_ids_of_2015 = set()
    season = 2015
    with open(matches_csv_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for match in matches_reader:
            if int(match["season"]) == season:
                match_ids_of_2015.add(int(match["id"]))
    return match_ids_of_2015


def get_top_economic_bowlers_of_season(deliveries_csv_path, ids_of_season):
    economy_by_bowlers = {}
    with open(deliveries_csv_path) as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        for delivery in deliveries_reader:
            if delivery["bowler"] not in economy_by_bowlers.keys() and int(delivery["match_id"]) in ids_of_season:
                economy_by_bowlers[delivery["bowler"]] = {
                    'total_runs': int(delivery["total_runs"]), 'total_balls': 1}
            elif int(delivery["match_id"]) in ids_of_season:
                economy_by_bowlers[delivery["bowler"]
                                   ]["total_runs"] += int(delivery["total_runs"])
                economy_by_bowlers[delivery["bowler"]]["total_balls"] += 1
    for bowler in economy_by_bowlers.keys():
        economy_by_bowlers[bowler] = (economy_by_bowlers[bowler]["total_runs"] /
                                      economy_by_bowlers[bowler]["total_balls"]) * 6
    return economy_by_bowlers


def plot_bar_chart():
    match_ids_of_2015 = get_match_ids_of_2015("matches.csv")
    economy_by_bowlers = get_top_economic_bowlers_of_season(
        "deliveries.csv", match_ids_of_2015)
    plt.bar([k for k in sorted(economy_by_bowlers, key=economy_by_bowlers.get, reverse=False)][: 10], [
            economy_by_bowlers[k] for k in sorted(economy_by_bowlers, key=economy_by_bowlers.get, reverse=False)][: 10])
    plt.title("Top economic Bowlers in 2015")
    plt.xlabel("Bowler")
    plt.ylabel("Economy")
    plt.show()

if __name__ == '__main__':
    plot_bar_chart()