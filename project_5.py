import csv
import matplotlib.pyplot as plt


def get_ids_by_season(matches_csv_path):
    ids_by_season = {}
    with open(matches_csv_path) as matches_file:
        matches_reader = csv.DictReader(matches_file)
        for match in matches_reader:
            ids_by_season[int(match["id"])] = int(match["season"])
    return ids_by_season


def get_sixes_per_season(deliveries_csv_path, ids_by_season):
    sixes_per_season = {}
    with open(deliveries_csv_path) as deliveries_file:
        deliveries_reader = csv.DictReader(deliveries_file)
        for delivery in deliveries_reader:
            if ids_by_season[int(delivery["match_id"])] not in sixes_per_season.keys() and int(delivery["batsman_runs"]) == 6:
                sixes_per_season[ids_by_season[int(delivery["match_id"])]] = 1
            elif int(delivery["batsman_runs"]) == 6:
                sixes_per_season[ids_by_season[int(delivery["match_id"])]] += 1
    return sixes_per_season


def plot_bar_chart():
    ids_by_season = get_ids_by_season("matches.csv")
    sixes_per_season = get_sixes_per_season("deliveries.csv", ids_by_season)
    plt.bar(range(len(sixes_per_season.keys())), [
            sixes_per_season[season] for season in sorted(sixes_per_season.keys())])
    plt.xticks(range(len(sixes_per_season.keys())),
               sorted(sixes_per_season.keys()))
    plt.title("Total number of sixes by Year")
    plt.xlabel("Season")
    plt.ylabel("Number of sixes")
    plt.show()


if __name__ == '__main__':
    plot_bar_chart()
