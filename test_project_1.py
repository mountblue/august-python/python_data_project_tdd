from project_1 import get_matches_per_season
import unittest


class TestProject1(unittest.TestCase):

    def test_get_matches_per_Season(self):

        expected_matches_per_season = {2016: 5, 2017: 4}
        current_csv = "test_data/project_1/test_1_matches.csv"
        calculated_matches_per_season = get_matches_per_season(current_csv)
        self.assertEqual(calculated_matches_per_season, expected_matches_per_season)

        expected_matches_per_season = {2008: 1, 2012: 1, 2016: 4, 2017: 3}
        current_csv = "test_data/project_1/test_2_matches.csv"
        calculated_matches_per_season = get_matches_per_season(current_csv)
        self.assertEqual(calculated_matches_per_season, expected_matches_per_season)

if __name__ == '__main__':
    unittest.main();