import project_2
import unittest


class TestProject2(unittest.TestCase):

    # Testing get_matches_won_by_all_teams_over_all_years function
    def test_get_matches_won_by_all_teams_over_all_years(self):

        # testing the function for test_1_matches csv file
        expected_matches_data_of_all_teams = {'Chennai Super Kings': {2008: 0, 2009: 0, 2010: 0, 2011: 0, 2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 3, 2017: 1},
                                              'Mumbai Indians': {2008: 0, 2009: 0, 2010: 0, 2011: 0, 2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 2, 2017: 3}
                                              }
        current_matches_csv = "test_data/project_2/test_1_matches.csv"
        calculated_matches_data_of_all_teams = project_2.get_matches_won_by_all_teams_over_all_seasons(
            current_matches_csv)
        self.assertEqual(expected_matches_data_of_all_teams,
                         calculated_matches_data_of_all_teams)

        # testing the function for test_2_matches csv file
        expected_matches_data_of_all_teams = {'Chennai Super Kings': {2008: 0, 2009: 0, 2010: 0, 2011: 0, 2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 2, 2017: 1},
                                              'Deccan Chargers': {2008: 0, 2009: 0, 2010: 0, 2011: 0, 2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 1, 2017: 0},
                                              'Kolkata Knight Riders': {2008: 0, 2009: 0, 2010: 0, 2011: 0, 2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 0, 2017: 1},
                                              'Mumbai Indians': {2008: 0, 2009: 0, 2010: 0, 2011: 0, 2012: 0, 2013: 0, 2014: 0, 2015: 0, 2016: 2, 2017: 2}
                                              }
        current_matches_csv = "test_data/project_2/test_2_matches.csv"
        calculated_matches_data_of_all_teams = project_2.get_matches_won_by_all_teams_over_all_seasons(
            current_matches_csv)
        self.assertEqual(expected_matches_data_of_all_teams,
                         calculated_matches_data_of_all_teams)



if __name__ == '__main__':
    unittest.main()
