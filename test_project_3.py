import project_3
import unittest


class TestProject3(unittest.TestCase):

    def test_get_match_ids_of_2016(self):

        expected_values = set([1, 2, 3, 4, 5])
        matches_path = "test_data/project_3/test_1_matches.csv"
        calculated_values = project_3.get_match_ids_of_2016(matches_path)
        self.assertEqual(expected_values, calculated_values)

        expected_values = set([1, 2, 5, 7])
        matches_path = "test_data/project_3/test_2_matches.csv"
        calculated_values = project_3.get_match_ids_of_2016(matches_path)
        self.assertEqual(expected_values, calculated_values)

    def test_get_extras_by_team(self):

        season_ids = {1, 2, 5, 7}
        expected_values = {'Chennai Super Kings': 0,
                           'Royal Challengers Bangalore': 3}
        deliveries_path = "test_data/project_3/test_1_deliveries.csv"
        calculated_values = project_3.get_extras_by_team(
            deliveries_path, season_ids)
        self.assertEqual(expected_values, calculated_values)



if __name__ == "__main__":
    unittest.main()
