import project_4
import unittest


class TestProject4(unittest.TestCase):

    def test_get_match_ids_of_2015(self):
        expected_ids = {1, 5}
        matches_csv_path = "test_data/project_4/test_1_matches.csv"
        calculated_ids = project_4.get_match_ids_of_2015(matches_csv_path)
        self.assertEqual(calculated_ids, expected_ids)

        expected_ids = {3}
        matches_csv_path = "test_data/project_3/test_2_matches.csv"
        calculated_ids = project_4.get_match_ids_of_2015(matches_csv_path)
        self.assertEqual(calculated_ids, expected_ids)

    def test_get_top_economic_bowlers_of_season(self):
        expected_values = {'TS Mills': 12.0, 'A Choudhary': 15.0}
        deliveries_csv_path = "test_data/project_3/test_1_deliveries.csv"
        ids_of_2015 = {1}
        calculated_values = project_4.get_top_economic_bowlers_of_season(
            deliveries_csv_path, ids_of_2015)
        self.assertEqual(calculated_values, expected_values)

        expected_values = {'TS Mills': 18.0, 'A Choudhary': 15.0}
        deliveries_csv_path = "test_data/project_3/test_2_deliveries.csv"
        ids_of_2015 = {1}
        calculated_values = project_4.get_top_economic_bowlers_of_season(
            deliveries_csv_path, ids_of_2015)
        self.assertEqual(calculated_values, expected_values)


if __name__ == '__main__':
    unittest.main()
