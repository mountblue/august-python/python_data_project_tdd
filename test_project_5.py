import project_5
import unittest


class TestProject5(unittest.TestCase):
    def test_get_ids_by_season(self):

        expected_ids_by_season = {1: 2016, 5: 2016, 2: 2016, 3: 2016, 4: 2016, 6: 2017, 7: 2017, 8: 2017, 9: 2017}
        matches_csv_path = "test_data/project_3/test_1_matches.csv"
        calculated_ids_by_season = project_5.get_ids_by_season(matches_csv_path)
        self.assertEqual(calculated_ids_by_season, expected_ids_by_season)

        expected_ids_by_season = {4: 2014, 3: 2015, 1: 2016, 2: 2016, 5: 2016, 7: 2016, 6: 2017, 9: 2017, 8: 2018}
        matches_csv_path = "test_data/project_3/test_2_matches.csv"
        calculated_ids_by_season = project_5.get_ids_by_season(matches_csv_path)
        self.assertEqual(calculated_ids_by_season, expected_ids_by_season)

    def test_get_sixes_per_season(self):

        expected_sixes_per_season = {2008: 2, 2009: 1}
        deliveries_csv_path = "test_data/project_3/test_1_deliveries.csv"
        ids_by_season = {1: 2008, 2: 2009}
        calculated_sixes_per_season = project_5.get_sixes_per_season(deliveries_csv_path, ids_by_season)
        self.assertEqual(calculated_sixes_per_season, expected_sixes_per_season)
        

if __name__ == "__main__":
    unittest.main()